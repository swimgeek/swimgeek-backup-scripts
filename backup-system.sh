#!/bin/bash
# Backup system and config files, /etc, /root etc
# With daily, weekly and monthly snapshots
# Ver 2019-10-22

# Will backup /etc and /root
#
# To install:
# create
# /home/backups/system/backup.daily
# /home/backups/system/backup.weekly
# /home/backups/system/backup.monthly
#
# add cron to:
# /etc/cron.d/backup-system
# Backup system at around 3am
# 25 03 * * * root /usr/local/sbin/backup-system.sh

####
# Set some variables and paths

BACKUP_DIR=home/backups/system
# Must contain backup.monthly backup.weekly backup.daily folders

BACKUP_DIRS="etc root usr/local/sbin"

email=root@localhost

######
# Make appropriate destination directory

# Destination file names
date_daily=`date +"%Y-%m-%d"`

# Get current month and week day number
month_day=`date +"%d"`
week_day=`date +"%u"`

# On first month day do monthly
if [ "$month_day" -eq 1 ] ; then
  destination=backup.monthly/$date_daily
else
  # On Saturdays do weekly
  if [ "$week_day" -eq 6 ] ; then
    destination=backup.weekly/$date_daily
  else
    # On any regular day do daily
    destination=backup.daily/$date_daily
  fi
fi

cd /$BACKUP_DIR
mkdir $destination

# latest backup symlink
ln -snf $destination /$BACKUP_DIR/latest

####
# Create backup files in today's directory

stamp=`date +%Y-%m-%d_%H-%M-%S`

# Compress files, one tar file per directory
for filedir in $BACKUP_DIRS; do
    tar -C / -cjf /$BACKUP_DIR/$destination/${filedir//\//-}.$stamp.tar.bz2 $filedir
done

#####
# Email backup info

ls -l /$BACKUP_DIR/$destination/ | mail -s "[`uname -n`] System Backup $date_daily" $email

#####
# Clean up old backups

# Daily - keep for 7 days
find /$BACKUP_DIR/backup.daily/ -maxdepth 1 -mtime +7 -type d -exec rm -rf {} \;

# Weekly - keep for 30 days
find /$BACKUP_DIR/backup.weekly/ -maxdepth 1 -mtime +30 -type d -exec rm -rf {} \;

# Monthly - keep for 365 days
find /$BACKUP_DIR/backup.monthly/ -maxdepth 1 -mtime +365 -type d -exec rm -rf {} \;

