Scripts to make daily, weekly and monthly snapshots and backups on
Debian servers

* System backups: configs etc
* Website backups: WordPress and MySQL databases

Add scripts to /usr/local/sbin and then add a cronjob to run them.
Create snapshot directories. See top of scripts for details.
